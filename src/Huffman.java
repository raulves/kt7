
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 * Lahenduses olen kasutanud allikat: https://github.com/herrbpl/i231-homework7/blob/master/src/Huffman.java
 */
public class Huffman {


   private Node[] leaves = null;
   private Node root = null;

   public class Node {
      int frequency;
      byte data;
      int bitLength;
      int bitmask;
      Node right = null;
      Node left = null;

      boolean isLeaf() {
         return (right == null && left == null);
      }

      // return data back as index;
      int index() {
         if (!this.isLeaf())
            return -1;
         return data & 0xFF;
      }

      @Override
      public String toString() {
         return String.format("%s %d %d", (char) data, data & 0xFF, frequency);
      }

      /**
       * Builds mapping table for decoding
       *
       * @param p
       * @param bm
       * @param bl
       * @param codeTable
       */
      void walkTree(String p, int bm, int bl, Node[] codeTable) {
         if (this.isLeaf()) {
            if (bl == 0)
               bl = bl + 1;
            this.bitLength = bl;
            this.bitmask = bm;
            codeTable[this.index()] = this;
         } else {
            if (this.left != null) {
               bm = bm << 1; // shift to left
               this.left.walkTree(p + "0", bm, bl + 1, codeTable);
            }
            if (this.right != null) {
               bm = bm + 1; // add single digit to end

               this.right.walkTree(p + "1", bm, bl + 1, codeTable);
            }
         }
      }

      /**
       * Finds if given input is found in tree leaves.
       * @param workArea - bit combination to search
       * @return -1 if not found, data byte as int when found (You must convert to byte)
       */
      public int findNode(int workArea) {

         int result;

         if (this.isLeaf()) {
            if (this.bitmask == workArea) return this.data;
         }

         if (this.left != null) {
            result = this.left.findNode(workArea);
            if (result != -1) return result;
         }
         if (this.right != null) {
            result = this.right.findNode(workArea);
            if (result != -1) return result;
         }

         return -1;
      }

      /**
       * Gets maximum code length in bits in frequency tree/table
       * @param i
       * @return
       */
      public int maxCodeLength(int i) {

         if (this.isLeaf()) {
            return i+1;
         }

         int l = i, r = i;
         if (this.left != null) {
            l = this.left.maxCodeLength(i+1);
         }
         if (this.right != null) {
            r = this.right.maxCodeLength(i+1);
         }
         return Math.max(l, r);
      }

   }

   // returns node with minimal frequency
   private int findMin(Node[] input) {
      int c = Integer.MAX_VALUE;
      int p = -1;
      for (int i = 0; i < input.length; i++) {
         if (input[i] != null && input[i].frequency < c) {
            c = input[i].frequency;
            p = i;
         }
      }
      return p;
   }

   // NB! byte is from -128 to 127.
   private Node[] buildFrequencyTable(byte[] data) {
      int[] frequency = new int[256];
      int count = 0;
      for (int i = 0; i < data.length; i++) {
         if (frequency[data[i] & 0xFF] == 0)
            count++;
         frequency[data[i] & 0xFF]++;
      }

      Node[] result = new Node[count];
      count = 0;

      for (int i = 0; i < frequency.length; i++) {
         if (frequency[i] > 0) {
            Node n = new Node();
            n.data = (byte) i;
            n.frequency = frequency[i];
            result[count++] = n;
         }
      }

      return result;
   }

   // returns root node of built tree.
   private Node buildTree(Node[] input) {

      boolean finished = false;

      Node[] workArray = input.clone();

      Node nl;
      Node nr;
      Node np;
      int min1;
      int min2;

      while (!finished) {
         // find first item
         min1 = findMin(workArray);
         if (min1 == -1) {
            return null;
         } else {
            nl = workArray[min1];
            workArray[min1] = null;

            // find min2
            min2 = findMin(workArray);
            if (min2 == -1) {
               return nl;
            } else {
               nr = workArray[min2];
               workArray[min2] = null;
               np = new Node();
               np.frequency = nl.frequency + nr.frequency;
               np.left = nl;
               np.right = nr;
               workArray[min1] = np;
            }
         }
      }
      return null;
   }

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      if (original == null) throw new RuntimeException("Empty input");
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      int resultBits = 0;
      for (int i = 0; i < leaves.length; i++) {
         if (leaves[i] != null) resultBits += leaves[i].bitLength * leaves[i].frequency;
      }
      return resultBits;
   }


   /** Encoding the byte array using this prefixCode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      // build frequency table
      Node[] nn = buildFrequencyTable(origData);

      // reset this leaves
      this.leaves = new Node[256];

      // build tree
      this.root = buildTree(nn);

      // create code table in leaves.
      this.root.walkTree("", 0, 0, this.leaves);

      // create output buffer
      byte[] result = new byte[this.calculateOutbytes()];

      int currentLength;
      int currentByte = 0;
      int position = 0;

      // into first three bits, we add length of padding bits in the end.
      currentLength = 3;

      Node n;
      // encode data
      for (int i = 0; i < origData.length; i++) {
         // get frequency and bitLength information
         n = this.leaves[origData[i] & 0xFF];
         if (n == null) throw new RuntimeException("Error in frequency table mapping!");
         int a = n.bitmask;

         if (n.bitLength + currentLength <= 8) {
            a = a << (8 - currentLength - n.bitLength);
            currentLength = currentLength + n.bitLength;
            currentByte = currentByte | a;
         } else {
            int over = -((8 - currentLength) - n.bitLength);

            a = a >>> over;
            currentByte = currentByte | a;
            result[position] = (byte) currentByte;
            position++;
            currentByte = (n.bitmask << (8 - over)) & 0x00FF;
            currentLength = over;
         }
      }

      int paddingLength = (8 - currentLength);

      paddingLength = paddingLength << 5;

      result[0] = (byte) (((int) result[0] | paddingLength));

      if (currentLength > 0) {
         result[position] = (byte) currentByte;
      }

      return result;
   }

   private int calculateOutbytes() {
      return (int) Math.ceil((this.bitLength() + 3) / 8.0);
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
      if (this.root == null) throw new RuntimeException("Dictionary missing, run encode first");

      if (encodedData.length == 0) return null;

      int currentCodeLength = 0;
      int workarea = 0;
      int maxCodeLength = root.maxCodeLength(0);
      int dataBitLength = this.bitLength();
      int bitCounter = 0;

      ArrayList<Byte> al = new ArrayList<Byte>();

      // iterate over bytes
      for (int i = 0; i < encodedData.length; i++) {
         int b = encodedData[i] & 0xFF;
         // iterate bit by bit, from most signifigant to least.
         for (int j = 7; j >= 0; j--) {
            bitCounter++;
            // only if we are not in header area or padding
            if ((bitCounter > 3) && ((bitCounter - 3) <= dataBitLength)) {
               boolean bit = ((b & (1 << j)) == (1 << j));
               if (currentCodeLength < maxCodeLength) {
                  currentCodeLength++;

                  workarea = workarea << 1; // move existing data to left
                  if (bit)
                     workarea = workarea + 1; // set last bit if set

                  int decodedbyte = root.findNode(workarea);
                  if (decodedbyte >= 0) {
                     al.add((byte)decodedbyte);
                     currentCodeLength = 0;
                     workarea = 0;
                  } else {
                  }
               } else {
                  throw new RuntimeException("Code length exceeds maximum code length in dictionary");
               }
            }
         }
      }

      byte[] result = new byte[al.size()];

      for (int i = 0; i < al.size(); i++) {
         result[i] = al.get(i);
      }
      return result;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }
}



